<?php

namespace yii2portal\core;

use yii\helpers\ArrayHelper;

abstract class Module extends \yii\base\Module
{

    public static function loadModulesConfig($dirPath)
    {
        $modules = [];
        $files = scandir($dirPath);
        foreach ($files as $file) {
            if (is_file("{$dirPath}/{$file}")) {
                $moduleName = str_replace(".php", '', $file);
                $moduleConfig = require("{$dirPath}/{$file}");
                if (file_exists("{$dirPath}/local/{$file}")) {
                    $moduleConfig = ArrayHelper::merge(
                        $moduleConfig,
                        require("{$dirPath}/local/{$file}")
                    );
                }

                $modules[$moduleName] = $moduleConfig;
            }
        }

        return $modules;
    }

    public function init()
    {
        parent::init();

        $class = new \ReflectionClass($this);
        $dir = dirname($class->getFileName());
        $configFile = $dir . '/config/main.php';
        if (file_exists($configFile)) {
            \Yii::configure($this, require($configFile));
        }
    }
}